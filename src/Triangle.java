
public class Triangle extends Shape {
    double sideB;
    double sideC;

    public Triangle(double sideA, double sideB, double sideC) {
        super(sideA);
        this.sideB = sideB;
        this.sideC = sideC;
    }

    public double triangleArea() {
        double S = (this.sideA + this.sideB + this.sideC) / 2;
        double A = (double) Math.sqrt(S * (S - this.sideA) * (S - this.sideB) * (S - this.sideC));
        return A;
    }
}