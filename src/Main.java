import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {

        Triangle tringle = new Triangle(4, 6, 9);
        Square square = new Square(4);
        Сircle сircle = new Сircle(6);
        System.out.println(" Площадь треугольника равна - " + tringle.triangleArea());
        System.out.println(" Площадь квадрата равна - " + square.squareArea());
        System.out.println(" Площадь круга равна - " + сircle.cirCleArea());
    }
}
