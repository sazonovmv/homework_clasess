public class Сircle extends Shape {
    public Сircle(double sideA) {
        super(sideA);
    }

    public double cirCleArea() {
        double S = Math.PI * super.sideA * 2;
        return S;
    }
}
