public class Square extends Shape {
    public Square(double sideA) {
        super(sideA);
    }

    public double squareArea() {
        double S = super.sideA * 2;
        return S;
    }
}

